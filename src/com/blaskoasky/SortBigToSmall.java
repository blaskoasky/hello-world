package com.blaskoasky;

import java.util.Arrays;
import java.util.Scanner;

public class SortBigToSmall {

    public static void main(String[] args) {

        int temp;
        int[] array = {6, 6, 5, 9, 2};

        for (int i = 0; i < array.length; i++) {
            for (int j = i; j < array.length; j++) {
                if (array[i] < array[j]) {
                    temp = array[i];
                    array[i] = array[j];
                    array[j] = temp;
                }
            }
        }

        System.out.println(Arrays.toString(array));
    }


}
